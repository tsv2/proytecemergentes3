﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager: MonoBehaviour
{
    //Turn this class into a Singleton
    private static PoolManager instance;
    public static PoolManager Instance
    {
        get
        {
            if(instance == null)
            Debug.LogError("PoolManager is NULL");

            return instance; 
        }
    }    
    
    [SerializeField]
    private GameObject bulletPrefab; 
    [SerializeField]
    private GameObject poolContainer;
    [SerializeField]
    private int poolSize = 5;
    [SerializeField]
    private List<GameObject> bulletPool = new List<GameObject>();

    [Header("Random pos")]
    public Transform minPos;
    public Transform maxPos;
    public Transform notMinPos, notMaxPos;
    private void Awake() 
    {
        instance = this;    
    }
    private void Start() 
    {
        CreatePool();    
    }
    //Create the bulletPool list
    private List<GameObject> CreatePool()
    {
        for (int i = 0; i < poolSize; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab, poolContainer.transform);
            bulletPool.Add(bullet);

            //POS
            bullet.transform.position = GetRandomPos();

            bullet.SetActive(true);
        }
        return bulletPool;
    }
    public GameObject RequestBullet() //Return a bullet from the bulletPool
    {
        foreach(var bullet in bulletPool)
        {
            if(bullet.activeSelf == false) //Check for inactive bullet
            {
                bullet.SetActive(true);

                //POS
                bullet.transform.position = GetRandomPos();

                return bullet;
            }
        }
        //Create a new bullet if all are active
        GameObject newBullet = Instantiate(bulletPrefab, poolContainer.transform);
        bulletPool.Add(newBullet);  

        //POS
        newBullet.transform.position = GetRandomPos();

        return newBullet;
    }
    private Vector2 GetRandomPos()
    {
        float x = Random.Range(minPos.position.x, maxPos.position.x);
        float y;
        //Top and Bottom rows
        if(x > notMinPos.position.x && x < notMaxPos.position.x)
        {
            int upOrDown = Random.Range(0, 2);
            if(upOrDown == 0)
            {
                y = Random.Range(notMinPos.position.y, minPos.position.y);
            }
            else
            {
                y = Random.Range(maxPos.position.y, notMaxPos.position.y);
            }
        }
        else //Right and left columns
        {
            y = Random.Range(maxPos.position.y, minPos.position.y);
        }

        return new Vector2(x, y);
    }
    //APPLES
    public void Portal_ChangeAllApples()
    {
        foreach (var a in bulletPool)
        {
            //Change color
            a.GetComponent<AppleController>().Change();
            //Change pos
            a.transform.position = GetRandomPos();
        }
    }
    public void RemoveAppleFromList(GameObject apple)
    {
        //Remove
        bulletPool.Remove(apple);
    }
    public void AddAppleToList(GameObject apple)
    {
        bulletPool.Add(apple);
        //set parent
        apple.transform.SetParent(poolContainer.transform);
        //set pos
        apple.transform.position = GetRandomPos();
    }
    public int CountNApplesOfNCurrentType()
    {
        int number = 0;
        foreach (var a in bulletPool)
        {
            if(a.GetComponent<AppleController>().nCurrent == ForestController.Instance.nCurrent)
            number++;
        }
        return number;
    }
}