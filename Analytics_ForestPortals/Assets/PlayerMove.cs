﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author> 
//https://github.com/CCheckley/Unity-Top-Down-Player-Movement-2D/blob/master/PlayerController.cs

// Ensure the component is present on the gameobject the script is attached to
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMove : MonoBehaviour
{
    // Local rigidbody variable to hold a reference to the attached Rigidbody2D component
    new Rigidbody2D rigidbody2D;

    public float movementSpeed = 1000.0f;

    public Animator anim;
    //Inputs
    private float inputH = 0f;
    private float inputV = 0f;

    void Awake()
    {
        // Setup Rigidbody for frictionless top down movement and dynamic collision
        rigidbody2D = GetComponent<Rigidbody2D>();

        rigidbody2D.isKinematic = false;
        rigidbody2D.angularDrag = 0.0f;
        rigidbody2D.gravityScale = 0.0f;
    }

    void Update()
    {
        //float inputH = Input.GetAxisRaw("Horizontal");
        //float inputV = Input.GetAxisRaw("Vertical");

        // Handle user input
        Vector2 targetVelocity = new Vector2(inputH, inputV);

        Move(targetVelocity);

        //Animator
        if(inputH != 0 || inputV != 0)
        {
            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }
    }

    void Move(Vector2 targetVelocity)
    {        
        // Set rigidbody velocity
        rigidbody2D.velocity = (targetVelocity * movementSpeed) * Time.deltaTime; // Multiply the target by deltaTime to make movement speed consistent across different framerates
    }

    //MOBILE CONTROLS
    public void OnTap_Horizontal(float value)
    {
        inputH = value;
    }
    public void OnTap_Vertical(float value)
    {
        inputV = value;
    }
}
