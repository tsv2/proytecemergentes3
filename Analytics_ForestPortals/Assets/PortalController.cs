﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalController : MonoBehaviour
{
    public int nCurent; //Value of portal... 1, 2, or 3
    public void PassThrough()
    {
        if(ForestController.Instance.nCurrent != nCurent) //Can't go back in same portal
        {
            PoolManager.Instance.Portal_ChangeAllApples();
            //Change forest
            ForestController.Instance.SetForest(nCurent);
        } 
    }
}
