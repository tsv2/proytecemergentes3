﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleController : MonoBehaviour
{
    public int nCurrent = 1;
    public Sprite[] sprites;
    SpriteRenderer renderer;

    private void Awake() 
    {
        renderer = GetComponent<SpriteRenderer>();
    }
    private void Start() 
    {
        UpdateSprite();
    }
    public void Change()
    {
        int formula = Mathf.Abs(nCurrent - ForestController.Instance.nCurrent);
        if(formula == 0)
            formula = 3;

        nCurrent = formula;
        Debug.Log("Formula " + formula);

        UpdateSprite();
    }
    private void UpdateSprite()
    {
        renderer.sprite = sprites[nCurrent - 1];
    }
}
