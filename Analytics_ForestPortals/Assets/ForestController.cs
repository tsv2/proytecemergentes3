﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestController : MonoBehaviour
{
    #region SINGLETON
    private static ForestController instance;
    public static ForestController Instance
    {
        get
        {
            if(instance == null)
            Debug.LogError("ForestController is NULL");

            return instance;
        }
    }
    #endregion
    public int nCurrent = 1;
    public Sprite[] sprites;
    SpriteRenderer renderer;

    private void Awake() 
    {
        instance = this; //Singleton 

        renderer = GetComponent<SpriteRenderer>();
    }
    private void Start() 
    {
        UpdateSprite();
    }

    public void SetForest(int value)
    {
        nCurrent = value;
        UpdateSprite();
    }
    
    private void UpdateSprite()
    {
        renderer.sprite = sprites[nCurrent - 1];
    }
}
