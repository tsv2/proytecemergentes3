﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIPanelsActivator : MonoBehaviour
{
    public GameObject[] panels; //All UI panels
    #region SINGLETON
    private static UIPanelsActivator instance;
    public static UIPanelsActivator Instance
    {
        get
        {
            if(instance == null)
            Debug.LogError("UIPanelsActivator is NULL");

            return instance; 
        }
    }    
    #endregion

    #region DEFAULT METHODS
    private void Start() 
    {
        instance = this; //Singleton
    }
    #endregion

    #region MY METHODS
    //Activates panel given in parameter. Deactivates all others
    public void ActivateOnlyThis(string panel)
    {
        foreach (var p in panels)
        {
            if(p.name == panel)
            {
                p.SetActive(true); //Activate panel
            }
            else
            {
                p.SetActive(false); //Deactivate all other panels
            }
            
        }
    }
    //Activates/Deactivates panel given in parameter. Ignores all others
    public void Activate(string panel, bool value)
    {
        foreach(var p in panels)
        {
            if(p.name == panel)
            {
                p.SetActive(value); //Activate/Deactivate panel
            }
        }
    }
    //Deactivates all panels
    public void DeActivateAll()
    {
        foreach(var p in panels)
        {
            p.SetActive(false);
        }
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }
    public void RestartScene()
    {
        SceneManager.LoadScene(0);
    }
    #endregion
}
