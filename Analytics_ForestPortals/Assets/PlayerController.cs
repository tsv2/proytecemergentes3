﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public List<GameObject> myApplesList = new List<GameObject>();
    
    //public Transform[] applePositions;
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.CompareTag("Portal"))
        {
            other.GetComponent<PortalController>().PassThrough();
            Invoke("LetApplesGo", 0.5f);
            AudioManager.Instance.Play("Portal");
        }    
        if(other.CompareTag("Apple"))
        {
            if(myApplesList.Count <= 2) //Collect max 3 apples
            {
                //Remove from pool list
                PoolManager.Instance.RemoveAppleFromList(other.gameObject);
                //Add to player list
                myApplesList.Add(other.gameObject);
                //Set pos
                other.transform.SetParent(transform, true);
                //other.GetComponent<AppleController>().SetPos(applePositions[myApplesList.Count-1]);
                //Audio
                AudioManager.Instance.Play("Apple");
            }
            
        }
    }
    //APPLES
    private void LetApplesGo()
    {
        int nApples = myApplesList.Count;
        for (int i = 0; i < nApples; i++)
        {
            PoolManager.Instance.AddAppleToList(myApplesList[0]);
            myApplesList.RemoveAt(0);
        }
        /*
        myApplesList.Clear(); 

        AppleController[] extraApples = transform.GetComponentsInChildren<AppleController>();
        foreach(var a in extraApples)
        {
            PoolManager.Instance.AddAppleToList(a.gameObject);
        }
        */
        //ui
        UIApplesTxt.Instance.UpdateUI();
    }
}
