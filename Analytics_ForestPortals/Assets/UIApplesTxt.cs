﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIApplesTxt : MonoBehaviour
{
    #region SINGLETON
    private static UIApplesTxt instance;
    public static UIApplesTxt Instance
    {
        get
        {
            if(instance == null)
            Debug.LogError("UIApplesTxt is NULL");

            return instance;
        }
    }
    #endregion

    public TextMeshProUGUI txtNApples;
    public int nTotalApples = 5;
    public GameObject winPanel;
    private void Awake() 
    {
        instance = this; //Singleton
    }
    private void Start() 
    {
        UpdateUI();
        winPanel.SetActive(false);
    }

    public void UpdateUI()
    {
        txtNApples.text = PoolManager.Instance.CountNApplesOfNCurrentType() + "/" + nTotalApples + " Apples good";
        //WIN GAME PANEL
        if(PoolManager.Instance.CountNApplesOfNCurrentType() >= nTotalApples)
        {
            Invoke("ShowWinPanel", 0.5f);
        }
    }
    private void ShowWinPanel()
    {
        winPanel.SetActive(true);
        //Audio
        AudioManager.Instance.Play("Win");
    }
}
